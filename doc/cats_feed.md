# how to feed cats at mascon

## tl;dr
weigh or eyeball approx 65g twice per day for each cat, there are five 

## the food
we use rawr, it comes frozen in ~1oz chunks, approximately 40pc to a bag 

it must be thawed out prior to serving, ideally in the fridge overnight but partly in the oven is ok

## procedure
this is what I do, you do not have to follow these instructions exactly. I've been doing this for a while though, and am pretty sure this is the easiest/fastest way to go about it

- take out the bowl with (hopefully) eleven pucks in it, dump them out on to the glass plate
- cut them in half or quarters or whatever with the spoon, they should be partly thawed and easy to cut
- put the plate (and the spoon) in the oven. do not turn the oven on, the pilot light keeps it pretty warm
- put eleven frozen pucks in the bowl for the next meal and put the bowl back in the fridge
- set a timer for 30m, or make a coffee, or watch a 30m video or something
- take the plate out and add enough water to make 713 grams, including the plate, mix it all up
- you can take the temperature if you want, there's a temp gun on the shelf under the kitchen table. I usually shoot for 18-20C but that is not totally necessary
- weigh out portions-- if you started with 316g (net) you should get
    - 65g mehitabel
    - 65g thor
    - 58g pris
    - 65g bubo
    - 65g baba yaga, or whatever's left over. you can use the white spatula/scraper to get most of the food off
- add one tiny scoop of plaque off (white container on top shelf of wire shelving) to mehitabel, pris and bubo's food
- add the contents of one lysine capsule (next to plaque off) to bubo's food
- mix up meds
- distribute plates:
    - thor and mehitabel will be the first to show up, and the most persistent
    - pris and bubo will be right behind them, I usually feed them by the sink on the rug there
    - put baba yaga's food out on her platform, watch out for fang lurking nearby
    - put the glass plate on the floor next to thor, they will clean off whatever you didn't scrape off
- watch to make sure no one eats someone else's food
- rinse and refill the water dish
- and that's it! easy, right?

## notes/tips

- it helps to put bubo and mehitabel (and thor) in the next room while you're prepping food. the door kind of sucks though, you kind of have to lift it to close it
- you can leave the plates on the floor for a few minutes, they will clean them off pretty thoroughly
- try to take note of how much they are eating, usually everyone will finish everything but not always
- bubo eats the slowest, pris eats medium slow. thor, baba yaga, and mehitabel normally eat very quickly

## the cats

bubo aka pudding, aka boobs is the biggest and (usually) the chillest. he is very luxurious. he also has a persistent upper respiratory issue and is snuffly most of the time, hence the lysine

pris aka ms cheeps, aka cheepy leepy is bubo's sister and has a similar head shape. she can be very snuggly but sometimes gets ganged up by everyone else

thor aka mr cheeps, aka athleticat has tons of energy, is very drooly, and will probably hide for the first few minutes any new person shows up. he usually comes out after a few minutes though

mehitabel aka hettie, aka hettie betty is very vocal when she wants to be. also kind of jumpy and likes to eat hair to get your attention

baba yaga aka babs lives outside and spends most of her time on the platform by the kitchen window. she has a heater for cold nights that I don't love leaving plugged in when no one is home but use your best judgement. if her towel gets soaked or disheveled please feel free to grab any other one and switch it out, the old one can go in the laundry basket or directly in the washing machine, as you see fit

fang aka fucking new guy is a recent adoptee of a guy who lives around the corner. I think he just wants to be friends but he really riles thor up and tries to steal baba yaga's food if you're not careful

## house stuff

- the window next to the sink will come completely out if it's not centered, be careful when you open and close it
- the cats are allowed pretty much everywhere except the office, which is the one room that has a working door that closes and latches. they like to hang out on the stove, which I don't love, but nothing bad has happened yet
- I like to leave the hallway light on, the front room (closes to streetside) light on, and the dining room light on. you can turn them off or on as you prefer, and all other lights are up to you
- I like to think the cats enjoy having the stereo on, in reality they probably don't care. whether you want to is up to you, just turn the knob up or down
